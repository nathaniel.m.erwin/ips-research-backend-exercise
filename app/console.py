from dotenv import load_dotenv

load_dotenv()
from controllers import fibonacci_controller as controller


def print_main_menu():
    print(
        """
            Choose Option:
            1. Compute Fibonacci Sequence
            2. Get Weekly Count
        """
    )


if __name__ == '__main__':
    print_main_menu()
    while True:
        response = input()
        if response == '1' or response == '2' or response == 'exit':
            break

    if response == '1':
        print('Enter the number (n) of fibonacci you want to calculate (limit 300). Type "exit" to quit at any time')
        while True:
            n = input()
            if n == 'exit':
                break
            sequence = controller.generate(int(n))
            print(sequence)
            controller.save_request(int(n), sequence)
    elif response == '2':
        print(
            'Enter the date in the form of MM-YYYY to generate a report of requests for that month. Type "exit" to '
            'quit at any time')
        while True:
            n = input()
            if n == 'exit':
                break
            report = controller.weekly_count(n)
            print(report)
