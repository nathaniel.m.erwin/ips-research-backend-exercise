from dotenv import load_dotenv
load_dotenv()

from flask import Flask
from routes import router


app = Flask(__name__)
app.register_blueprint(router)


@app.route("/")
def welcome() -> str:
    return 'Welcome to the API!'


@app.errorhandler(404)
def page_not_found(e):
    return 'Error, that page does not exist', 404

if __name__ == '__main__':
    app.run(debug=True)
