from controllers.fibonacci_controller import generate, save_request, weekly_count
from database import db_connection
import unittest
from datetime import datetime


class FibonacciControllerTest(unittest.TestCase):

    def test_generate(self):
        self.assertEqual(generate(-41), 0, 'anything less than 0 should return 0')
        self.assertEqual(generate(0), 0)
        self.assertEqual(generate(1), '0-1')
        self.assertEqual(generate(6), '0-1-1-2-3-5')

        with self.assertRaises(Exception):
            generate(301)

        with self.assertRaises(TypeError):
            generate('should only support int')

    def test_save_request(self):
        n, sequence = 10, '0-1-1-2-3-5-8-13-21-34'
        save_request(n, sequence)

        statement = '''SELECT n_terms, sequence
            FROM fibonacci
            ORDER BY time_submitted DESC 
            LIMIT 1
        '''
        connection, cursor = db_connection()
        cursor.execute(statement)
        value = cursor.fetchone()

        self.assertEqual(value['n_terms'], n)
        self.assertEqual(value['sequence'], sequence)

    def test_weekly_report(self):
        with self.assertRaises(Exception):
            generate('01-12-41')

        empty_result = weekly_count('01-1990')

        for x in empty_result['summary']:
            self.assertEqual(x['count'], 0)

        month = datetime.now().month
        year = datetime.now().year
        non_empty_result = weekly_count('{:02}-{}'.format(month, year))

        is_not_empty = [x for x in non_empty_result['summary'] if x['count'] != 0]
        self.assertGreater(len(is_not_empty), 0)


if __name__ == '__main__':
    unittest.main()
