from database import db_connection
import re
import calendar


def generate(n: int) -> str:
    if n > 300:
        raise Exception('Only values less than or equal to 300 are allowed')
    if n <= 0:
        return 0
    sequence = [0, 1]

    for x in range(2, n):
        sequence.append(sequence[x - 1] + sequence[x - 2])

    return "-".join(str(n) for n in sequence)


def save_request(n: int, sequence: str) -> None:
    statement = """INSERT INTO
           fibonacci(n_terms, sequence, time_submitted)
           VALUES (%s, %s, NOW());"""

    try:
        connection, cursor = db_connection()
        cursor.execute(statement, (n, sequence))
        connection.commit()
        cursor.close()
        connection.close()
    except BaseException as error:
        print('Error', error)
        raise


def weekly_count(date: str) -> dict:
    pattern = re.compile('^((0[1-9])|(1[0-2]))-([0-9]{4})$')
    if not pattern.match(date):
        raise Exception('Invalid input must be in form of MM-YYYY')

    month, year = date.split('-')

    statement = """SELECT json_agg(result) AS summary
        FROM (SELECT 
        1 + FLOOR((EXTRACT(DAY FROM 
        date_trunc('week', time_submitted + interval '1 day') - interval '1 day') - 1) / 7) AS week,
        count(id) AS count
        FROM fibonacci
        WHERE date_part('year', time_submitted) = {} AND date_part('month',time_submitted) = {}
        GROUP BY 1
        ORDER BY week DESC) result""".format(year, month)

    try:
        connection, cursor = db_connection()
        cursor.execute(statement)
        records = cursor.fetchall()
        cursor.close()
        connection.close()
        response = records[0]

        if not response['summary']:
            response['summary'] = []

        calendar.setfirstweekday(calendar.SUNDAY)
        number_of_weeks = len(calendar.monthcalendar(int(year), int(month)))

        for x in range(1, number_of_weeks + 1):
            exists = ([i for i in response['summary'] if i['week'] == x] or [None])[0]
            if not exists:
                response['summary'].append({'week': x, 'count': 0})

        return response
    except BaseException as error:
        print('Error', error)
        raise
