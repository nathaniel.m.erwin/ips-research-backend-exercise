import psycopg2
from psycopg2.extras import RealDictCursor
import os

config = {
    'host': os.environ.get('POSTGRES_HOST'),
    'user': os.environ.get('POSTGRES_USER'),
    'dbname': os.environ.get('POSTGRES_DB'),
    'password': os.environ.get('POSTGRES_PASSWORD')
}


def db_connection():
    connection = psycopg2.connect(**config)
    cursor = connection.cursor(cursor_factory=RealDictCursor)
    return connection, cursor
