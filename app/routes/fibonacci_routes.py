from . import router
import controllers.fibonacci_controller as controller


@router.route("/<n>")
def calculate(n: str) -> str:
    sequence = controller.generate(int(n))
    controller.save_request(n, sequence)
    return sequence


@router.route("/requests/<month_year>")
def requests(month_year: str) -> dict:
    return controller.weekly_count(month_year)
