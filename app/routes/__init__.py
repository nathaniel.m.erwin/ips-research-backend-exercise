from flask import Blueprint

router = Blueprint('router', __name__, url_prefix='/fibonacci')
from . import fibonacci_routes
