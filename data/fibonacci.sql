CREATE TABLE IF NOT EXISTS fibonacci (
  id serial PRIMARY KEY,
  n_terms INT NOT NULL,
  sequence TEXT,
  time_submitted TIMESTAMP NOT NULL
);

CREATE INDEX idx_fibonacci_time_submitted
ON fibonacci (time_submitted);