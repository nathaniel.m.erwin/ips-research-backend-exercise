# IPS Research Backend Exercise

## Summary

For this programming exercise, you are going to create a simple API which calculates a Fibonacci Sequence for "N" Terms. The result will be stored in a database and returned to the caller.  

The API should run on localhost with the tech stack specified below. Docker should host both the Postgres database and a python web server.  The python web server should be custom code created by you.  You may use any web frameworks such as Flask, Web2py, etc.   Your project must contain unit tests. This is your chance to show us your unit testing capabilities.  In addition to the API project, create a separate console application for manually sending input to the API. You are NOT allowed to use an ORM to query the database.

## Details

### Tech Stack

- Python 3.6 or later
- Postgres
- Docker

### Requirements
1. Create your own Fork of the excersice repository, build out the api in your fork, then submit a Merge Request to push your folk back to the excersice repo.
2. API Functionality
    1. Calculate Fibonacci Sequence
        - URL - /fibonacci/{n}
        - This endpoint calculates the Fibonacci sequence for "n" terms
            - The result should be stored in a Postgres table
                - Table Data should contain at least the following columns
                    - n_terms
                    - sequence
                    - time_submitted
                - The "sequence" for the Fibonacci sequence should contain all terms in the sequence concatenated together as a single string, delimited by a "-".  For example, if n=6, sequence="0-1-1-2-3-5"

    2. Return a summary for monthly requests
        - URL - /fibonacci/requests/{month_year}
        - This endpoint returns a weekly count for the month and year
            - A week starts on Sunday and ends on Saturday
            - If there were not any requests for a given week, return count=0
            - Only include a week number if applicable for the month in question.  In the example above, May 2020 spans 6 weeks.  If you were returning a summary for February 2020, your summary would return weeks 1 through 5 only.
            - Example:
                - month_year = "05-2020"
                - database contains entries for the following dates:
                    - 5/1/2020
                    - 5/2/2020
                    - 5/3/2020
                    - 5/5/2020
                    - 5/9/2020
                    - 5/19/2020
                    - 5/30/2020
                    - 5/30/2020
                - Expected Result: 
                    ```json
                    {
                        "summary": [
                            {"week": 1, "count": 2},
                            {"week": 2, "count": 3},
                            {"week": 3, "count": 0},
                            {"week": 4, "count": 1},
                            {"week": 5, "count": 2},
                            {"week": 6, "count": 0}
                        ]
                    }
                    ```

### Extra Credit
1. Add a Gitlab CI/CD Pipeline configuration that performs the following tasks for every push to the remote repository:
    1. Runs Unit Test
    2. Builds your custom API docker image

## Running the Application
Before getting started create a .env file at root with contents of .env.example or rename the .env.example file. Now
within root run ``docker compose up`` the app will run on localhost:5000 with the following routes:
1. localhost:5000/fibonacci/<n> where n is integer from -inf to 300. Going beyond 300 should give you an error.
2. localhost:5000/requests/<month_year> where month_year is in the form of MM-YYYY
3. A console app is also including should first initialize venv ```source venv/bin/activate``` and then run with the command: ```python console.py```

## Unit tests
Unit tests have be added to run the suite run the following command from app root:

```python -m unittest discover -s . -p '*_test.py'```